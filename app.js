const express = require('express');
const { ApolloServer, gql } = require('apollo-server-express');
const ApolloServerConfig = require('./schema/');
const app = express();

const server = new ApolloServer(ApolloServerConfig);

server.applyMiddleware({ app, path: '/graphql' });

app.listen({ port: 8000 }, () => {
  require('./config/db')(); // connecting to the database;
  console.log('Apollo Server on http://localhost:8000/graphql');
});
