const [userSchema, userResolver] = require('./User');

module.exports = {
  typeDefs: [userSchema],
  resolvers: [userResolver],
};
