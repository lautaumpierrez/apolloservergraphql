const { gql } = require('apollo-server-express');
const User = require('../models/User');
const UserDef = gql`
  type User {
    _id: ID!
    name: String
    surname: String
    email: String
    password: String
    createdAt: String
    updatedAt: String
  }
  type Query {
    user(id: ID!): User
    users: [User]
  }
  type Mutation {
    addUser(name: String,surname: String,email: String,password: String,createdAt: String,updatedAt: String): User
  }
`;
const resolvers = {
  Query: {
    user: async (parent, args) => {
      const user = await User.findById(args.id);
      return user;
    },
    users:async ()=>{
      const users = await User.find({});
      return users;
    }
  },
  Mutation: {
    addUser: async (root, args, context) => {
      const user = new User(args);
      await user.save();
      return user;
    },
  },
};
module.exports = [UserDef, resolvers];
