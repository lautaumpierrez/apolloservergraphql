const mongoose = require('mongoose');
module.exports = async function () {
  await mongoose.connect(
    'mongodb://lautaumpierrez:lauti123456@ds045531.mlab.com:45531/graphql-try',
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  );
};
