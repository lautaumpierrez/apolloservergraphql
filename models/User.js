const { Schema, model } = require('mongoose');

const UserSchema = new Schema(
  {
    name: String,
    surname: String,
    email: String,
    password: String,
    createdAt: String,
    updatedAt: String,
  },
  { versionKey: false }
);

module.exports = model('User', UserSchema);
